/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.gate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.service.gate.constants.Constants;
import org.ow2.weblab.service.gate.converter.GateConverter;
import org.ow2.weblab.service.gate.converter.GateToWookie;
import org.purl.dc.elements.DublinCoreAnnotator;

import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.creole.ResourceInstantiationException;

/**
 * This class is a WebService calling Gate with different configurations. It initialises gate once with a gapp configuration and implements a corpus pipeline.
 * Most of the parameters are set in the converter.
 *
 * @see GateConverter
 * @author khelif, ymombrun
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public final class GateService implements Analyser {


	/*
	 * TODO Check how we can handle the trouble with language and snowball porter stemmer. The trouble is that in snowball language codes are defined in the plugin, and they cannot be bypassed.
	 */
	private static final String GATE_LANGUAGE_FEATURE = "language";


	/**
	 * The unknown language.
	 */
	private static final Set<String> UNKNOWN = new HashSet<>(Arrays.asList("", "unk", "und", "UNK", "UND", "UNKNOWN"));


	/**
	 * The converter in charge of transferring information extracted by Gate into the input resource.
	 */
	private final GateConverter converter;


	/**
	 * The number of seconds to wait between when trying to poll a controller
	 */
	private final int pollingTimeoutInSeconds;


	/**
	 * The logger
	 */
	private final Log logger;


	/**
	 * @param controllerTemplate
	 *            The corpus controller to be duplicated (or used)
	 */
	public GateService(final CorpusController controllerTemplate) {
		this(controllerTemplate, new GateToWookie(), 60, 1);
	}


	/**
	 * @param controllerTemplate
	 *            The template of controller to be used for feeding the blocking queue of controllers
	 * @param converter
	 *            The GateConverter in charge of reading Gate documents and annotate WebLab texts
	 * @param pollingTimeoutInSeconds
	 *            The number of seconds to wait when trying to poll an instance
	 * @param nbControllers
	 *            The number of controller to be added to the list of controllers
	 */
	public GateService(final CorpusController controllerTemplate, final GateConverter converter, final int pollingTimeoutInSeconds, final int nbControllers) {
		super();
		this.logger = LogFactory.getLog(this.getClass());
		this.converter = converter;
		this.pollingTimeoutInSeconds = pollingTimeoutInSeconds;
		if (!GateControllerPool.resetControllers(controllerTemplate, nbControllers)) {
			throw new WebLabUncheckedException("Not a single controller instanciated.");
		}
		this.logger.info("Gate Service successully initialised using " + GateControllerPool.maxSize() + " controller(s).");
	}



	@Override
	public ProcessReturn process(final ProcessArgs args) throws UnexpectedException, InvalidParameterException, InsufficientResourcesException {
		// Tests the input parameters and get every Text section contained by the resource in args.
		final List<Text> texts = this.checkParameters(args);

		final Resource resource = args.getResource();
		final String source = new DublinCoreAnnotator(resource).readSource().firstTypedValue();
		if (texts.isEmpty()) {
			this.logger.warn(Constants.SERVICE_NAME + ": unable to properly process document '<" + resource.getUri() + ">' - '<" + source + ">'. No text content found in resource. Nothing done.");
			final ProcessReturn theRet = new ProcessReturn();
			theRet.setResource(resource);
			return theRet;
		}

		this.logger.debug(Constants.SERVICE_NAME + ": start processing document '<" + resource.getUri() + ">' - '<" + source + ">'");

		final String docDefaultLanguage = this.getLanguage(resource, null);


		// For each Text section
		for (final Text text : texts) {
			this.logger.trace("Text section to process by GateAnalyserComponent: " + text.getUri());
			this.logger.trace("Number of segments before GateAnalyserComponent: " + text.getSegment().size());

			final Map<gate.Document, Text> gateDocsAndText = new HashMap<>();

			// Instantiates an empty Gate Corpus
			final Corpus corpusGate;
			try {
				corpusGate = Factory.newCorpus(resource.getUri() + " " + System.nanoTime());
			} catch (final ResourceInstantiationException rie) {
				final String message = Constants.SERVICE_NAME + ": error processing document '<" + resource.getUri() + ">' - '<" + source + ">'. Unable to instanciate corpus needed.";
				this.logger.error(message, rie);
				throw ExceptionFactory.createUnexpectedException(message, rie);
			}
			try {

				// Creates an empty Gate Document
				final gate.Document docGate;
				try {
					docGate = Factory.newDocument(text.getContent());
				} catch (final ResourceInstantiationException rie) {
					this.logger.warn(Constants.SERVICE_NAME + ": unable to properly process document '<" + resource.getUri() + ">' - '<" + source
							+ ">'. Unable to create a new Gate Document for text '" + text.getUri() + ". Skipping it.", rie);
					continue;
				}

				// Extract the language of the text and add this as feature in the Gate Document.
				final String language = this.getLanguage(text, docDefaultLanguage);
				if (language != null) {
					docGate.getFeatures().put(GateService.GATE_LANGUAGE_FEATURE, language);
				}

				// Add the Gate doc in the corpus and maps it to its WebLab Text.
				corpusGate.add(docGate);
				gateDocsAndText.put(docGate, text);

				GateControllerPool.processCorpus(corpusGate, text.getUri(), this.pollingTimeoutInSeconds);

				this.logger.trace("Answer received from Gate. Starting annotation convertion for Resource '" + resource.getUri() + "'.");

				// Extract annotations of each Gate Document and add them to the WebLab Text.
				this.converter.convertInformation(corpusGate, resource, gateDocsAndText);
			} finally {
				// Clean up the resources (might have already been done in the converter)
				final Iterator<Entry<gate.Document, Text>> it = gateDocsAndText.entrySet().iterator();
				while (it.hasNext()) {
					final Document doc = it.next().getKey();
					doc.cleanup();
					corpusGate.unloadDocument(doc);
					Factory.deleteResource(doc);
					it.remove();
				}
				gateDocsAndText.clear();
				corpusGate.cleanup();
				Factory.deleteResource(corpusGate);
			}
		}

		texts.clear();

		this.logger.debug(Constants.SERVICE_NAME + ": end processing document '<" + resource.getUri() + ">' -'<" + source + ">'.");

		// Creates the return wrapper and add the resource in it.
		final ProcessReturn theRet = new ProcessReturn();
		theRet.setResource(resource);
		return theRet;
	}


	/**
	 * @param args
	 *            The ProcessArgs
	 * @return The list of Text contained by the Resource in args.
	 * @throws InvalidParameterException
	 *             For any reason preventing the retrieval of text unit to be done.
	 */
	protected List<Text> checkParameters(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String message = Constants.SERVICE_NAME + ": error processing document '<null>' - '<null>'. ProcessArgs was null.";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}

		final Resource res = args.getResource();
		if (res == null) {
			final String message = Constants.SERVICE_NAME + ": error processing document '<null>' - '<null>'. Resource in ProcessArgs was null.";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}
		if (!(res instanceof MediaUnit)) {
			final String message = Constants.SERVICE_NAME + ": error processing document '<null>' - '<null>'. This service only process MediaUnit; Resource was a: " + res.getClass().getSimpleName()
					+ ".";
			this.logger.error(message);
			throw ExceptionFactory.createInvalidParameterException(message);
		}

		final List<Text> texts;
		if (res instanceof Text) {
			texts = new LinkedList<>();
			texts.add((Text) res);
		} else {
			texts = ResourceUtil.getSelectedSubResources(args.getResource(), Text.class);
		}

		for (final ListIterator<Text> textIt = texts.listIterator(); textIt.hasNext();) {
			final Text text = textIt.next();
			if ((text.getContent() == null) || text.getContent().replaceAll("\\s+", "").isEmpty()) {
				textIt.remove();
			}
		}

		return texts;
	}


	/**
	 * @param resource
	 *            A resource to be read
	 * @param defaultValue
	 *            The default language value to be used
	 * @return The language or the default one if not found (may be null)
	 */
	private String getLanguage(final Resource resource, final String defaultValue) {
		final String language;
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(resource);
		final Value<String> langValue = dca.readLanguage();
		final String source = dca.readSource().firstTypedValue();
		if (langValue.hasValue()) {
			language = langValue.firstTypedValue();
			if (langValue.size() > 1) {
				this.logger.warn(Constants.SERVICE_NAME + ": unable to properly process document '<" + resource.getUri() + ">'" + source + ">'" + "More than one language found on " + resource.getUri()
						+ ". Using first one.");
			}
		} else {
			language = defaultValue;
		}
		if (GateService.UNKNOWN.contains(language)) {
			this.logger.trace("Language " + language + " is a code for unknown language. Using default value.");
			return defaultValue;
		}
		return language;
	}

}
