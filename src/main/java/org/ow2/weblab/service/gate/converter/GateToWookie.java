/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.gate.converter;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.comparator.SegmentComparator;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.extended.util.TextUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.rdf.Value;
import org.ow2.weblab.service.gate.constants.Constants;
import org.purl.dc.terms.DCTermsAnnotator;

import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;

/**
 * This class is the basic implementation GateConverter.
 * It enables the creation of WOOKIE instances of NamedEntities in each Text section of the input WebLab Resource.
 *
 * @author lserrano, ymombrun
 */
public class GateToWookie implements GateConverter {


	private static final String WOOKIE_NS = "http://weblab.ow2.org/wookie#";


	/**
	 * The URI of the service to be added in each annotation created (or null if nothing shall be added).
	 */
	private URI serviceURI;


	/**
	 * The first part of the URI of the created instances.
	 */
	private static final String INSTANCE_URI_PREFIX = "http://weblab.ow2.org/wookie/instances/";


	/**
	 * The logger used inside
	 */
	protected final Log log;


	/**
	 * The defautl constructor
	 */
	public GateToWookie() {
		this(null);
	}


	/**
	 * @param serviceURI
	 *            The service uri to be annotated in documents
	 */
	public GateToWookie(final String serviceURI) {
		if (serviceURI == null) {
			this.serviceURI = null;
		} else {
			this.serviceURI = URI.create(serviceURI);
		}
		this.log = LogFactory.getLog(this.getClass());
	}


	@Override
	public void convertInformation(final Corpus corpusGate, final Resource resource, final Map<Document, Text> gateDocsAndText) {

		final Iterator<Entry<gate.Document, Text>> it = gateDocsAndText.entrySet().iterator();
		while (it.hasNext()) {
			final Map.Entry<gate.Document, Text> entry = it.next();
			this.linkGateAnnotsToText(entry.getValue(), entry.getKey().getAnnotations());

			this.log.trace("Number of segment after GateExtractionComponent: " + entry.getValue().getSegment().size());
			if (this.log.isTraceEnabled()) {
				try {
					this.log.trace(ResourceUtil.saveToXMLString(entry.getValue()));
				} catch (final WebLabCheckedException wlce) {
					this.log.warn(
							Constants.SERVICE_NAME + ": unable to properly process document '<" + resource.getUri() + ">'. Error serialising to XML the resource: '" + entry.getValue().getUri() + "'.",
							wlce);
				}
			}

			// Empties the memory for each doc
			corpusGate.unloadDocument(entry.getKey());
			entry.getKey().cleanup();
			Factory.deleteResource(entry.getKey());
			it.remove();
		}

		// Clears the map to get memory back
		gateDocsAndText.clear();

		// Empties the memory from the corpus
		corpusGate.cleanup();
		Factory.deleteResource(corpusGate);
	}


	/**
	 * Annotate text with each annotation in annotation set.
	 * At the end, sorts the segments list to ease further process.
	 *
	 * @param text
	 *            The WebLab Text to be annotated
	 * @param annots
	 *            The Gate annotation set to be used to annotate text
	 */
	private void linkGateAnnotsToText(final Text text, final AnnotationSet annots) {
		int nbAnnots = 0;

		// Creates the annotation that will contains the information extracted from Gate
		final Annotation wlAnnot = WebLabResourceFactory.createAndLinkAnnotation(text);

		/*
		 * Creates the annotator that will be used.
		 */
		final WProcessingAnnotator wpa = new WProcessingAnnotator(URI.create(text.getUri()), wlAnnot);

		// Add is producedBy statement if needed
		if (this.serviceURI != null) {
			final URI wlAnnotUri = URI.create(wlAnnot.getUri());
			wpa.startInnerAnnotatorOn(wlAnnotUri);
			wpa.writeProducedBy(this.serviceURI);
			wpa.endInnerAnnotator();
			new DCTermsAnnotator(wlAnnotUri, wlAnnot).writeCreated(Calendar.getInstance().getTime());
		}

		if (this.log.isDebugEnabled()) {
			this.log.trace("Gate Annotation set: " + annots);
		}

		// For each annotation in the annotation Set
		for (final gate.Annotation gateAnnot : annots) {
			if (gateAnnot.getType().equals("Person")) {
				this.linkGateAnnotToText(text, gateAnnot, wpa, "Person");
				nbAnnots++;
			} else if (gateAnnot.getType().equals("Organization")) {
				this.linkGateAnnotToText(text, gateAnnot, wpa, "Unit");
				nbAnnots++;
			} else if (gateAnnot.getType().equals("Location")) {
				this.linkGateAnnotToText(text, gateAnnot, wpa, "Place");
				nbAnnots++;
			}
		}

		if (nbAnnots > 0) {
			// Sort segments in the right order to have a better usability
			Collections.sort(text.getSegment(), new SegmentComparator());
		} else {
			// Remove useless annotation
			text.getAnnotation().remove(wlAnnot);
		}

	}


	/**
	 * Creates a <code>LinearSegment</code> at the position of the <code>gate.Annotation</code>.
	 * Creates an instance of this entity using the PoKHelper.
	 *
	 * @param text
	 *            The text section to process
	 * @param annotGate
	 *            An annotation in gate format
	 * @param wpa
	 *            The annotator to be used to create instances.
	 */
	private void linkGateAnnotToText(final Text text, final gate.Annotation annotGate, final WProcessingAnnotator wpa, final String annotWookieType) {
		// Creates the segment from start and end of the Gate Annotation
		final LinearSegment segment = SegmentFactory.createAndLinkLinearSegment(text, annotGate.getStartNode().getOffset().intValue(), annotGate.getEndNode().getOffset().intValue());

		// The type of the RDF instance
		final URI typeURI = URI.create(GateToWookie.WOOKIE_NS + annotWookieType);
		/*
		 * Try to retrieve the label from the text content and create an URI from it.
		 * If it's throws an exception, it means that the segment is not properly set and need to be removed.
		 */
		final String label;
		final URI instanceURI;
		try {
			label = TextUtil.getSegmentText(text, segment).replaceAll("\\s+", " ").trim();
			instanceURI = this.getUriFromLabel(GateToWookie.INSTANCE_URI_PREFIX + annotWookieType + '#', label);
		} catch (final WebLabCheckedException wlce) {
			this.log.warn(Constants.SERVICE_NAME + ": unable to properly process document '<" + wpa.getSubject() + ">'. Unable to retrieve text at segment: " + segment.getUri() + " - "
					+ segment.getStart() + " - " + segment.getEnd() + ". Removing it.", wlce);
			text.getSegment().remove(segment);
			return;
		}

		// Add simple statements: refersTo, then type and label.
		wpa.startInnerAnnotatorOn(URI.create(segment.getUri()));
		wpa.writeRefersTo(instanceURI);
		wpa.endInnerAnnotator();

		wpa.startInnerAnnotatorOn(instanceURI);
		final Value<URI> existingType = wpa.readType();
		if (existingType.hasValue() && existingType.getValues().contains(typeURI)) {
			// Already annotated with the same type, nothing to do
		} else {
			if (existingType.hasValue()) {
				this.log.warn(Constants.SERVICE_NAME + ": unable to properly process document '<" + wpa.getSubject() + ">'. " + "Entity " + instanceURI + " is polymorphic. Types are "
						+ existingType.getValues() + " and " + typeURI.toASCIIString() + ".");
			}
			wpa.writeType(typeURI);
		}

		if (!label.isEmpty()) {
			final Value<String> existingLabel = wpa.readLabel();
			if (existingLabel.hasValue() && existingLabel.getValues().contains(label)) {
				// Already annotated with the same label, nothing to do
			} else {
				if (existingLabel.hasValue()) {
					this.log.warn(Constants.SERVICE_NAME + ": unable to properly process document '<" + wpa.getSubject() + ">'. Entity " + instanceURI + " has various labels. Values are "
							+ existingLabel.getValues() + " and " + label + ".");
				}
				wpa.writeLabel(label);
			}
		}

	}


	private URI getUriFromLabel(final String baseUri, final String label) {
		final StringBuilder sb = new StringBuilder();
		for (final char c : label.toCharArray()) {
			if (Character.isLetterOrDigit(c)) {
				sb.append(c);
			} else {
				sb.append(' ');
			}
		}
		final String cleanedLabel = sb.toString().replaceAll("\\s+", " ").trim().replace(' ', '_').toLowerCase();
		String uri;
		try {
			uri = new URL(baseUri + cleanedLabel).toURI().toString();
		} catch (final URISyntaxException urise) {
			this.log.warn(Constants.SERVICE_NAME + ": unable to properly process document '<null>'. Unable to transform the label '" + label + "' into an URI.", urise);
			uri = baseUri + System.nanoTime();
		} catch (final MalformedURLException murle) {
			this.log.warn(Constants.SERVICE_NAME + ": unable to properly process document '<null>'. Unable to transform the label '" + label + "' into aan URI.", murle);
			uri = baseUri + System.nanoTime();
		}
		return URI.create(uri);
	}

}
