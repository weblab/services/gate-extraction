/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.gate;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.services.InsufficientResourcesException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.service.gate.constants.Constants;

import gate.Corpus;
import gate.CorpusController;
import gate.Factory;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;



/**
 * This class should be used as a Singleton in order to handle the handling of Gate Resources.
 *
 * @author ymombrun
 */
public final class GateControllerPool {


	/**
	 * The singleton instance of {@link GateControllerPool}.
	 */
	private static GateControllerPool instance = new GateControllerPool();


	/**
	 * The pool of controllers to be used inside.
	 */
	private final BlockingQueue<CorpusController> controllers = new LinkedBlockingQueue<>();


	/**
	 * The lock to be used to prevent from having reading (i.e. processing using controllers in the blockingqueue) during writing (i.e. reseting the controllers)
	 */
	private final ReadWriteLock rwLock = new ReentrantReadWriteLock(true);


	/**
	 * The logger used by this class
	 */
	private final Log logger;


	/**
	 * Current size of the blocking queue when all workers are available.
	 */
	private int maxSize = 0;


	/**
	 * The default constructor that should not be used.
	 */
	private GateControllerPool() {
		super();
		this.logger = LogFactory.getLog(this.getClass());
		this.logger.debug(Constants.SERVICE_NAME + " Singleton instance of GateControllerPool created.");
	}


	/**
	 * Poll an instance of {@link CorpusController} (waiting at max <code>secondsToWait</code> seconds), and process the provided {@link Corpus} with it.
	 * Uses both a reading lock mechanism to synchronise with the reset of the controllers and the blocking queue to prevent from using twice the same controller in parallel.
	 *
	 * @param corpus
	 *            The Gate corpus to process
	 * @param resourceId
	 *            The uri of the WebLab document that has been converted to a corpus. Used inside logging messages.
	 * @param secondsToWait
	 *            When polling an instance of {@link CorpusController}. When waiting more that <code>secondsToWait</code>, an error is thrown.
	 * @throws UnexpectedException
	 *             If an error occurs during the execution of the pipeline.
	 * @throws InsufficientResourcesException
	 *             If an error occured when trying to retrieve an instance of pipeline to process the corpus.
	 */
	public static void processCorpus(final Corpus corpus, final String resourceId, final int secondsToWait) throws InsufficientResourcesException, UnexpectedException {
		GateControllerPool.instance._processCorpus(corpus, resourceId, secondsToWait);
	}


	/**
	 * @param controllerTemplate
	 *            The template to be used as a baseline for duplicating the {@link CorpusController}.
	 * @param nbControllers
	 *            The number of {@link CorpusController} to create and to put in the controllers queue.
	 * @return Wether or not the list of controllers contains at least one controller at the end
	 */
	public static boolean resetControllers(final CorpusController controllerTemplate, final int nbControllers) {
		return GateControllerPool.instance._resetControllers(controllerTemplate, nbControllers);
	}


	/**
	 * @return The size of the controller list when created
	 */
	public static int maxSize() {
		return GateControllerPool.instance.getMaxSize();
	}


	private void _processCorpus(final Corpus corpus, final String resourceId, final int secondsToWait) throws InsufficientResourcesException, UnexpectedException {

		// 1. Get a reading lock
		Thread.yield();
		this.logger.trace("Try to acquire a reading lock to process " + resourceId + ".");
		final Lock readLock = this.rwLock.readLock();
		final long start = System.currentTimeMillis();
		final boolean acquired;
		try {
			acquired = readLock.tryLock(secondsToWait, TimeUnit.SECONDS);
		} catch (final InterruptedException ie) {
			final String msg = Constants.SERVICE_NAME + ": error processing document '<" + resourceId + ">'. Unable to acquire reading lock before polling an instance of controller to process.";
			this.logger.error(msg, ie);
			throw ExceptionFactory.createInsufficientResourcesException(msg, ie);
		}
		try {
			// 2. Check if acquired and if enough time remains available for waiting when polling
			if (acquired) {
				this.logger.trace("Reading lock acquired to process " + resourceId + ".");
			} else {
				final String msg = Constants.SERVICE_NAME + ": error processing document '<" + resourceId + ">'. Unable to acquire reading lock before polling an instance of controller to process.";
				this.logger.error(msg);
				throw ExceptionFactory.createInsufficientResourcesException(msg);
			}
			final long remaingTimeToWait = (1 + secondsToWait) - ((System.currentTimeMillis() - start) / 1000);
			if (remaingTimeToWait < 1) {
				final String msg = Constants.SERVICE_NAME + ": error processing document '<" + resourceId + ">'. Unable to acquire reading lock before polling an instance of controller to process.";
				this.logger.error(msg);
				throw ExceptionFactory.createInsufficientResourcesException(msg);
			}

			// 2. Polling a pipeline
			this.logger.debug("Try to acquire a controller to process " + resourceId + ".");
			final CorpusController controller;
			try {
				controller = this.controllers.poll(remaingTimeToWait, TimeUnit.SECONDS);
			} catch (final InterruptedException ie) {
				final String msg = Constants.SERVICE_NAME + ": error processing document '<" + resourceId + ">'. Unable to retrieve an instance of controller to process.";
				this.logger.error(msg, ie);
				throw ExceptionFactory.createInsufficientResourcesException(msg, ie);
			}
			if (controller == null) {
				final String msg = Constants.SERVICE_NAME + ": error processing document '<" + resourceId + ">'. Unable to retrieve an instance of controller to process.";
				this.logger.error(msg);
				throw ExceptionFactory.createInsufficientResourcesException(msg);
			}
			this.logger.trace("Controller acquired to process " + resourceId + ". Start processing.");


			// 3. Processing corpus using the pipeline
			try {
				controller.setCorpus(corpus);
				controller.execute();
			} catch (final ExecutionException ee) {
				final String msg = Constants.SERVICE_NAME + ": error processing document '<" + resourceId + ">'. An exception occured during processing.";
				this.logger.error(msg, ee);
				throw ExceptionFactory.createUnexpectedException(msg, ee);
			} finally {
				// In any case re add the pipeline to the worker queue
				controller.setCorpus(null);
				this.controllers.add(controller);
			}
		} finally {
			// In any case release the write lock
			readLock.unlock();
			this.logger.trace("Releasing reading lock acquired for " + resourceId + ".");
			Thread.yield();
		}
	}



	/**
	 * @param controllerTemplate
	 *            The controller template to be used to reload new controllers
	 * @param nbControllers
	 *            The number of controller to create after reset
	 * @return <code>true</code> if at least one controller has been
	 */
	public boolean _resetControllers(final CorpusController controllerTemplate, final int nbControllers) {

		// 1. Getting a write lock
		Thread.yield();
		this.logger.trace("Try to acquire writing lock, in order to (re)set controllers. There are currently " + this.controllers.size() + " controller(s) in the queue.");
		final Lock writeLock = this.rwLock.writeLock();
		writeLock.lock();
		this.logger.trace("Write lock acquired. There are currently " + this.controllers.size() + " controller(s) in the queue.");

		try {
			int failed = 0;

			// 2. Releasing all controllers
			while (!this.controllers.isEmpty() || (failed > (nbControllers + 1))) {
				Thread.yield();
				final CorpusController controller = this.controllers.poll();
				if (controller == null) {
					this.logger.warn(Constants.SERVICE_NAME + ": unable to properly process document. No controller retrieved while write lock has been acquired. This is strange. Retry.");
					failed++;
				} else {
					this.logger.trace("Controller acquired for removal. Start cleaning it up.");
					// Clean up that controller
					Thread.yield();
					controller.cleanup();
					Factory.deleteResource(controller);
					System.gc();
				}
			}

			// 3. Duplicating the template as many times as required
			for (int i = 0; i < nbControllers; i++) {
				this.logger.trace("Calling controller duplication.");
				final CorpusController controller;
				try {
					controller = (CorpusController) Factory.duplicate(controllerTemplate);
				} catch (final ResourceInstantiationException rie) {
					this.logger.warn(Constants.SERVICE_NAME + ": unable to properly process document. An error occured duplicating a controller.", rie);
					continue;
				}
				this.controllers.add(controller);
				this.logger.trace("Controller duplicated.");
			}

			this.setMaxSize(this.controllers.size());

			return !this.controllers.isEmpty();
		} finally {

			// 4. Releasing the lock in any case
			writeLock.unlock();
			this.logger.trace("Releasing writing lock.");
		}
	}


	/**
	 * @return the maxSize
	 */
	public int getMaxSize() {
		return this.maxSize;
	}


	/**
	 * @param maxSize
	 *            the maxSize to set
	 */
	public void setMaxSize(final int maxSize) {
		this.maxSize = maxSize;
	}


	@Override
	protected void finalize() throws Throwable {
		this._resetControllers(null, 0);
		super.finalize();
	}

}
