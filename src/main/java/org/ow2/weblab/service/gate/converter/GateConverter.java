/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.gate.converter;

import gate.Corpus;

import java.util.Map;

import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;


/**
 * This interface has to be implemented by any specific converter.
 * A converter is used to handle annotations added on a Gate Document by the controller, to pass them to the original WebLab Text resource.
 *
 * Givent the fact that the RealtimeCorpusController implemation of gate seem to be likely to produce OutOfMemroy errors. We will in fact always have a map with a single Text. This enable to have
 * slight changes only.
 *
 * @author ymombrun
 * @see GateToWookie The default implementation that converts to the Wookie Ontology
 * @see GateToWebLab Another implementation that could be used
 */
public interface GateConverter {


	/**
	 * This is the only method of the interface. It has to iterate on the entries in the Map to pass information from
	 * the key to the value.
	 *
	 * @param corpus
	 *            A Gate Corpus that has been processed by the controller.
	 * @param resource
	 *            The top level Resource. May be used to annotate it, or not.
	 * @param gateDocsAndText
	 *            A map that associate to every gate.Document in the Corpus a WebLab Text to whom pass the annotation.
	 */
	public void convertInformation(final Corpus corpus, final Resource resource, final Map<gate.Document, Text> gateDocsAndText);

}
