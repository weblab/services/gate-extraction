/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.gate.converter;

import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;

import java.net.URI;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.extended.comparator.SegmentComparator;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.properties.PropertiesLoader;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.extended.util.TextUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.service.gate.constants.Constants;
import org.purl.dc.terms.DCTermsAnnotator;


/**
 * This class is the basic implementation GateConverter.
 * It enables the creation of simple (and sometimes stupid) instances of NamedEntities in each Text section of the input
 * WebLab Resource.
 * At initialisation, it loads some properties from the file PROPERTIES_FILE_NAME:
 * <ul>
 * <li>SKIPPED_ANNOTATIONS_PROPERTY: list of properties to be excluded from the conversion. Typically it contains SpaceToken, Token... and every other Gate Annotation that will not be used further in
 * WebLab document. If none, nothing will be skipped.</li>
 * <li>SKIPPED_FEATURES_PROPERTY: list of features to be excluded from the conversion. Typically it contains rules, matches... and every other Gate Annotation that will not be used further in WebLab
 * document. If none, nothing will be skipped.</li>
 * <li>SERVICE_URI_PROPERTY: the URI of the service to be used if the created annotation shall contains a isProducedBy statement. If not defined, no statement will be added.</li>
 * <li>MAX_ANNOTS_PROPERTY: the max number of annotation to be extracted on a single Text. If not defined (or negative number), no limit will be used.</li>
 * <li>INCLUDE_FEATURES_PROPERTY: whether or not to include features in addition to annotation type. If not defined, features will not be included.</li>
 * <li>INCLUDE_IDS_PROPERTY: whether or not to include id's properties in output annotations. If not defined, ids will not be included</li>
 * </ul>
 *
 * @author ymombrun
 */
public class GateToWebLab implements GateConverter {


	/**
	 * Name of the property file to be loaded.
	 */
	public final static String PROPERTIES_FILE_NAME = "gatetoweblab.properties";


	/**
	 * Name of the property that contains the list of properties to be excluded from the conversion. Typically it
	 * contains SpaceToken, Token... and every other Gate Annotation that will not be used
	 * further in WebLab document.
	 */
	public final static String SKIPPED_ANNOTATIONS_PROPERTY = "skippedAnnotations";


	/**
	 * Name of the property that contains the list of features to be excluded from the conversion. Typically it contains
	 * rules, matches... and every other Gate Annotation that will not be used further
	 * in WebLab document.
	 */
	public final static String SKIPPED_FEATURES_PROPERTY = "skippedFeatures";


	/**
	 * Name of the property that contains the URI of the service to be used if the created annotation shall contains a
	 * isProducedBy statement.
	 */
	public final static String SERVICE_URI_PROPERTY = "serviceUri";


	/**
	 * Name of the property that contains the max number of annotation to be extracted on a single MediaUnit.
	 */
	public final static String MAX_ANNOTS_PROPERTY = "maxNumberOfAnnots";


	/**
	 * Name of the property that defines whether or not to include features in addition to annotation type.
	 */
	public final static String INCLUDE_FEATURES_PROPERTY = "includeFeatures";


	/**
	 * Name of the property that defines whether or not to include id's properties in output annotations.
	 */
	public final static String INCLUDE_IDS_PROPERTY = "includeIds";


	/**
	 * The prefix to be used to replace the Gate URI in RDF
	 */
	private final static String GATE_PREFIX = "gate";


	/**
	 * The base URI of the created instances instances.
	 */
	private final static String GATE_URI = "http://gate.ac.uk/gatemodel#";


	/**
	 * The first part of the URI of the created instances.
	 */
	private static final String INSTANCE_URI_PREFIX = "weblab:gateInstance/";


	/**
	 * The property referring the end node Id of an annotation.
	 */
	private static final URI END_NODE_ID = URI.create(GateToWebLab.GATE_URI + "endNodeId");


	/**
	 * The property referring the start node Id of an annotation.
	 */
	private static final URI START_NODE_ID = URI.create(GateToWebLab.GATE_URI + "startNodeId");


	/**
	 * The property referring the node Id of an annotation.
	 */
	private static final URI NODE_ID = URI.create(GateToWebLab.GATE_URI + "nodeId");


	/**
	 * Excluded annotations. Read in PROPERTIES_FILE_NAME using EXCLUSION_PROPERTY.
	 */
	private final Set<String> excludedAnnotations;


	/**
	 * Excluded annotations. Read in PROPERTIES_FILE_NAME using FEATURE_EXCLUSION_PROPERTY.
	 */
	private final Set<String> excludedFeatures;


	/**
	 * Whether or not to add features. Read in PROPERTIES_FILE_NAME using INCLUDE_FEATURES_PROPERTY.
	 */
	private final boolean addFeatures;


	/**
	 * Whether or not to add ids. Read in PROPERTIES_FILE_NAME using VERBOSE_PROPERTY.
	 */
	private final boolean addIds;


	/**
	 * The URI of the service to be added in each annotation created (or null if nothing shall be added). Read in
	 * PROPERTIES_FILE_NAME using SERVICE_URI_PROPERTY.
	 */
	private final URI serviceURI;


	/**
	 * The maximum number of annotation to be extracted on a single MediaUnit (or a null or negative number if no
	 * maximum is neeeded). Read in PROPERTIES_FILE_NAME using MAX_ANNOTS_PROPERTY.
	 */
	private final int maxNumberOfAnnots;


	private final Log log;


	/**
	 * The default constructors
	 * Reads PROPERTIES_FILE_NAME to set every internal properties.
	 */
	public GateToWebLab() {
		final Map<String, String> props = PropertiesLoader.loadProperties(GateToWebLab.PROPERTIES_FILE_NAME, GateToWebLab.class);
		final String serviceURIStr = props.get(GateToWebLab.SERVICE_URI_PROPERTY);
		if (serviceURIStr == null) {
			this.serviceURI = null;
		} else {
			this.serviceURI = URI.create(serviceURIStr);
		}
		final String excludedString = props.get(GateToWebLab.SKIPPED_ANNOTATIONS_PROPERTY);
		this.log = LogFactory.getLog(this.getClass());
		if ((excludedString != null) && !excludedString.isEmpty()) {
			this.excludedAnnotations = new HashSet<>(Arrays.asList(excludedString.split(";")));
		} else {
			this.log.warn(Constants.SERVICE_NAME + ": unable to properly create service. No " + GateToWebLab.SKIPPED_ANNOTATIONS_PROPERTY + " property found in " + GateToWebLab.PROPERTIES_FILE_NAME
					+ " file; nothing will be skipped.");
			this.excludedAnnotations = Collections.emptySet();
		}

		final String excludedFeaturesString = props.get(GateToWebLab.SKIPPED_FEATURES_PROPERTY);
		if ((excludedFeaturesString != null) && !excludedFeaturesString.isEmpty()) {
			this.excludedFeatures = new HashSet<>(Arrays.asList(excludedFeaturesString.split(";")));
		} else {
			this.log.warn(Constants.SERVICE_NAME + ": unable to properly create service. No " + GateToWebLab.SKIPPED_FEATURES_PROPERTY + " property found in " + GateToWebLab.PROPERTIES_FILE_NAME
					+ " file; nothing will be skipped.");
			this.excludedFeatures = Collections.emptySet();
		}

		final String includeFeatures = props.get(GateToWebLab.INCLUDE_FEATURES_PROPERTY);
		if (includeFeatures != null) {
			this.addFeatures = Boolean.parseBoolean(includeFeatures);
		} else {
			this.log.warn(Constants.SERVICE_NAME + ": unable to properly create service. No " + GateToWebLab.INCLUDE_FEATURES_PROPERTY + " property found in " + GateToWebLab.PROPERTIES_FILE_NAME
					+ " file; Features will not be added.");
			this.addFeatures = false;
		}

		final String verboseMode = props.get(GateToWebLab.INCLUDE_IDS_PROPERTY);
		if (verboseMode != null) {
			this.addIds = Boolean.parseBoolean(verboseMode);
		} else {
			this.log.warn(Constants.SERVICE_NAME + ": unable to properly create service. No " + GateToWebLab.INCLUDE_IDS_PROPERTY + " property found in " + GateToWebLab.PROPERTIES_FILE_NAME
					+ " file; Verbose mode will not be activated.");
			this.addIds = false;
		}

		final String maxNumberOfSegmentsString = props.get(GateToWebLab.MAX_ANNOTS_PROPERTY);
		if (maxNumberOfSegmentsString != null) {
			int value;
			try {
				value = Integer.parseInt(maxNumberOfSegmentsString);
				if (value == 0) {
					value = -1;
				}
			} catch (final NumberFormatException nfe) {
				this.log.warn(Constants.SERVICE_NAME + ": unable to properly create service. Unable to parse " + GateToWebLab.MAX_ANNOTS_PROPERTY + " property found in "
						+ GateToWebLab.PROPERTIES_FILE_NAME + ". Read value was: '" + maxNumberOfSegmentsString + "'; No limit will be used.", nfe);
				value = -1;
			}
			this.maxNumberOfAnnots = value;
		} else {
			this.log.warn(Constants.SERVICE_NAME + ": unable to properly create service. No " + GateToWebLab.MAX_ANNOTS_PROPERTY + " property found in " + GateToWebLab.PROPERTIES_FILE_NAME
					+ " file; No limit will be used.");
			this.maxNumberOfAnnots = -1;
		}

		props.clear();
	}


	@Override
	public void convertInformation(final Corpus corpusGate, final Resource resource, final Map<Document, Text> gateDocsAndText) {

		final Iterator<Entry<gate.Document, Text>> it = gateDocsAndText.entrySet().iterator();
		while (it.hasNext()) {
			final Map.Entry<gate.Document, Text> entry = it.next();
			this.linkGateAnnotsToText(entry.getValue(), entry.getKey().getAnnotations());

			this.log.trace("Number of segment after GateExtractionComponent: " + entry.getValue().getSegment().size());
			if (this.log.isTraceEnabled()) {
				try {
					this.log.trace(ResourceUtil.saveToXMLString(entry.getValue()));
				} catch (final WebLabCheckedException wlce) {
					this.log.warn(Constants.SERVICE_NAME + ": unable to properly process document '<" + resource.getUri() + ">'. Unable to serialise to XML the resource: '" + entry.getValue().getUri()
							+ "'.", wlce);
				}
			}

			// Empties the memory for each doc
			corpusGate.unloadDocument(entry.getKey());
			Factory.deleteResource(entry.getKey());
			it.remove();
		}

		// Clears the map to get memory back
		gateDocsAndText.clear();

		// Empties the memory from the corpus
		Factory.deleteResource(corpusGate);
	}


	/**
	 * Annotate text with each annotation in annotation set.
	 * At the end, sorts the segments list to ease further process.
	 *
	 * @param text
	 *            The WebLab Text to be annotated
	 * @param annots
	 *            The Gate annotation set to be used to annotate text
	 */
	private void linkGateAnnotsToText(final Text text, final AnnotationSet annots) {
		int nbAnnots = 0;

		// Creates the annotation that will contains the information extracted from Gate
		final Annotation wlAnnot = WebLabResourceFactory.createAndLinkAnnotation(text);

		/*
		 * Creates the helper that will be used.
		 */
		final WProcessingAnnotator wpa = new WProcessingAnnotator(URI.create(text.getUri()), wlAnnot);

		// Add is producedBy statement if needed
		if (this.serviceURI != null) {
			final URI wlAnnotUri = URI.create(wlAnnot.getUri());
			wpa.startInnerAnnotatorOn(wlAnnotUri);
			wpa.writeProducedBy(this.serviceURI);
			wpa.endInnerAnnotator();
			new DCTermsAnnotator(wlAnnotUri, wlAnnot).writeCreated(Calendar.getInstance().getTime());
		}

		this.log.trace("Gate Annotation set: " + annots);

		// For each annotation in the annotation Set
		for (final gate.Annotation gateAnnot : annots) {
			// If the annotation is not of a skipped type
			if (!this.excludedAnnotations.contains(gateAnnot.getType())) {
				// If the maxNumberOfAnnots is not reached yet (or if not limit has been set)
				if ((this.maxNumberOfAnnots < 0) || (nbAnnots <= this.maxNumberOfAnnots)) {
					this.linkGateAnnotToText(text, gateAnnot, wpa);
					nbAnnots++;
				} else {
					this.log.warn(Constants.SERVICE_NAME + ": unable to properly process document. Too many annotations created on a single media-unit (" + nbAnnots + "). Return as it is.");
					break;
				}
			}
		}

		if (nbAnnots > 0) {
			// Sort segments in the right order to have a better usability
			Collections.sort(text.getSegment(), new SegmentComparator());
		} else {
			// Remove useless annotation
			text.getAnnotation().remove(wlAnnot);
		}
	}


	/**
	 * Creates a <code>LinearSegment</code> at the position of the <code>gate.Annotation</code>.
	 * Creates an instance of this entity using the PoKHelper.
	 *
	 * @param text
	 *            The text section to process
	 * @param annotGate
	 *            An annotation in gate format
	 * @param pokh
	 *            The pokHelper yo be used to create instances.
	 */
	private void linkGateAnnotToText(final Text text, final gate.Annotation annotGate, final WProcessingAnnotator wpa) {
		// Creates the segment from start and end of the Gate Annotation
		final LinearSegment segment = SegmentFactory.createAndLinkLinearSegment(text, annotGate.getStartNode().getOffset().intValue(), annotGate.getEndNode().getOffset().intValue());

		// The random URI to be used by the RDF instance to be created
		final URI instanceURI = URI.create(GateToWebLab.INSTANCE_URI_PREFIX + annotGate.getType() + '#' + System.nanoTime());

		// The type of the RDF instance
		final URI typeURI = URI.create(GateToWebLab.GATE_URI + annotGate.getType());

		/*
		 * Try to retrieve the label from the text content.
		 * If it's throws an exception, it means that the segment is not properly set and need to be removed.
		 */
		final String label;
		try {
			label = TextUtil.getSegmentText(text, segment);
		} catch (final WebLabCheckedException wlce) {
			this.log.warn(Constants.SERVICE_NAME + ": unable to properly process document. Unable to retrieve text at segment: " + segment.getUri() + " - " + segment.getStart() + " - "
					+ segment.getEnd() + ". Removing it.", wlce);
			text.getSegment().remove(segment);
			return;
		}

		// Add simple statements: refersTo, then type and label.
		wpa.startInnerAnnotatorOn(URI.create(segment.getUri()));
		wpa.writeRefersTo(instanceURI);
		wpa.endInnerAnnotator();

		wpa.startInnerAnnotatorOn(instanceURI);
		wpa.writeType(typeURI);
		if (!label.trim().isEmpty()) {
			wpa.writeLabel(label.trim());
		}

		/*
		 * If the features are need, this code tries to convert every entry of the feature map into a good RDF statement (using GATE_URI + the key as predicate) and having the String representation of
		 * the value as Literal.
		 */
		if (this.addFeatures) {
			final FeatureMap featureMap = annotGate.getFeatures();
			if ((featureMap != null) && !featureMap.isEmpty()) {
				for (final Entry<Object, Object> entry : featureMap.entrySet()) {
					if (entry.getKey() instanceof String) {
						final String featureName = ((String) entry.getKey()).trim();
						if ((!this.excludedFeatures.contains(featureName)) && (entry.getValue() != null)
								&& (!(entry.getValue() instanceof Collection<?>) || !((Collection<?>) entry.getValue()).isEmpty())) {
							wpa.applyOperator(Operator.WRITE, GateToWebLab.GATE_PREFIX, URI.create(GateToWebLab.GATE_URI + featureName), String.class, entry.getValue().toString());
						}
					} else {
						this.log.warn(Constants.SERVICE_NAME + ": unable to properly process document. Unable to create feature from entry '" + entry + "' on gate annotation type '"
								+ annotGate.getType() + "'.");
					}
				}
			}
		}

		/*
		 * If ids are needed, it adds nodeId, startNodeId and endNodeId
		 */
		if (this.addIds) {
			wpa.applyOperator(Operator.WRITE, GateToWebLab.GATE_PREFIX, GateToWebLab.NODE_ID, String.class, annotGate.getId().toString());
			wpa.applyOperator(Operator.WRITE, GateToWebLab.GATE_PREFIX, GateToWebLab.START_NODE_ID, String.class, annotGate.getStartNode().getId().toString());
			wpa.applyOperator(Operator.WRITE, GateToWebLab.GATE_PREFIX, GateToWebLab.END_NODE_ID, String.class, annotGate.getEndNode().getId().toString());
		}
		wpa.endInnerAnnotator();
	}

}
