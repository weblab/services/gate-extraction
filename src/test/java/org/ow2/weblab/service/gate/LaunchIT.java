/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.gate;

import gate.CorpusController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.helper.ResourceHelper;
import org.ow2.weblab.core.helper.impl.JenaResourceHelper;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.context.support.FileSystemXmlApplicationContext;


/**
 * Test the Gate Extraction service in a MultiThreaded environment.
 *
 * This test will not be executed by the 'test' goal but by the 'verify' one who is fired after install.
 *
 * @author EADS IPCC Team
 */
public class LaunchIT {


	private final static int MIN_TEXTS_BY_DOCS = 1;


	private final static int MAX_TEXTS_BY_DOCS = 5;


	private final static int NB_THREAD = 25;



	@Test
	public void testGateInThread() throws Exception {
		execGateInThreads("src/test/resources/beans.xml");
	}


	@Test
	public void testGateRTInThread() throws Exception {
		execGateInThreads("src/test/resources/beans-RT.xml");
	}


	@Test
	public void testGateInThreadWithReseter() throws Exception {
		this.execGateInThreadWithReseters("src/test/resources/beans.xml");
	}


	@Test
	public void testGateRTInThreadWithReseter() throws Exception {
		this.execGateInThreadWithReseters("src/test/resources/beans-RT.xml");
	}




	protected void execGateInThreads(final String beans) throws Exception {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext(beans)) {
			final GateService ga = fsxac.getBean("analyserImpl", GateService.class);

			final List<ResourceHelper> rhs = this.getRHs();

			final List<Callable<Resource>> analysers = new ArrayList<>();

			LogFactory.getLog(this.getClass()).info("Begin thread creation.");
			for (int i = 1; i <= LaunchIT.NB_THREAD; i++) {
				final Document newDoc = WebLabResourceFactory.createResource("GateService", "Thread-" + i, Document.class);

				final int nbTexts = (int) ((Math.random() * ((LaunchIT.MAX_TEXTS_BY_DOCS + 1) - LaunchIT.MIN_TEXTS_BY_DOCS)) + LaunchIT.MIN_TEXTS_BY_DOCS);

				for (int j = 0; j < nbTexts; j++) {
					final ResourceHelper rh = rhs.get((int) (Math.random() * (rhs.size())));
					final Document doc = (Document) rh.getResource();

					final Text t = WebLabResourceFactory.createAndLinkMediaUnit(newDoc, Text.class);
					t.setContent(((Text) doc.getMediaUnit().get(0)).getContent());
					new DublinCoreAnnotator(t).writeLanguage(rh.getLitsOnPredSubj(doc.getUri(), DublinCore.LANGUAGE_PROPERTY_NAME).get(0));
				}

				final AnalyserMultiThread thread = new AnalyserMultiThread(ga, newDoc, null);
				analysers.add(thread);
			}

			LogFactory.getLog(this.getClass()).info("Threads created. Invoking all.");

			final ExecutorService executor = Executors.newFixedThreadPool(LaunchIT.NB_THREAD);
			final List<Future<Resource>> results = executor.invokeAll(analysers, 5L, TimeUnit.MINUTES);

			for (final Future<Resource> future : results) {
				Assert.assertTrue(future.isDone());
				Assert.assertFalse(future.isCancelled());
				Assert.assertNotNull(future.get());
			}

			LogFactory.getLog(this.getClass()).info("Thread executed.");

		}
	}




	private void execGateInThreadWithReseters(String beans) throws Exception {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext(beans)) {
			final GateService ga = fsxac.getBean("analyserImpl", GateService.class);

			final List<ResourceHelper> rhs = this.getRHs();

			final List<Callable<Resource>> analysers = new ArrayList<>();
			final List<Callable<Boolean>> reseters = new ArrayList<>();

			LogFactory.getLog(this.getClass()).info("Begin thread creation.");
			for (int i = 1; i <= LaunchIT.NB_THREAD; i++) {
				final Document newDoc = WebLabResourceFactory.createResource("GateService", "Thread-" + i, Document.class);

				final int nbTexts = (int) ((Math.random() * ((LaunchIT.MAX_TEXTS_BY_DOCS + 1) - LaunchIT.MIN_TEXTS_BY_DOCS)) + LaunchIT.MIN_TEXTS_BY_DOCS);

				for (int j = 0; j < nbTexts; j++) {
					final ResourceHelper rh = rhs.get((int) (Math.random() * (rhs.size())));
					final Document doc = (Document) rh.getResource();

					final Text t = WebLabResourceFactory.createAndLinkMediaUnit(newDoc, Text.class);
					t.setContent(((Text) doc.getMediaUnit().get(0)).getContent());
					new DublinCoreAnnotator(t).writeLanguage(rh.getLitsOnPredSubj(doc.getUri(), DublinCore.LANGUAGE_PROPERTY_NAME).get(0));
				}

				final AnalyserMultiThread thread = new AnalyserMultiThread(ga, newDoc, null);
				analysers.add(thread);

				final ResetMultiThread resetThread = new ResetMultiThread(fsxac.getBean("myController", CorpusController.class), 2, Math.random() > 0.5);
				reseters.add(resetThread);
			}

			LogFactory.getLog(this.getClass()).info("Thread created.");

			final ExecutorService executor2 = Executors.newFixedThreadPool(LaunchIT.NB_THREAD);
			final List<Future<Resource>> results2 = executor2.invokeAll(analysers, 5L, TimeUnit.MINUTES);

			final ExecutorService executor1 = Executors.newFixedThreadPool(LaunchIT.NB_THREAD);
			final List<Future<Boolean>> results1 = executor1.invokeAll(reseters, 5L, TimeUnit.MINUTES);

			for (final Future<Boolean> future : results1) {
				Assert.assertTrue(future.isDone());
				Assert.assertFalse(future.isCancelled());
				Assert.assertTrue(future.get().booleanValue());
			}
			int i = 1;
			for (final Future<Resource> future : results2) {
				Assert.assertTrue(future.isDone());
				Assert.assertFalse(future.isCancelled());
				new WebLabMarshaller().marshalResource(future.get(), new File("target/gen" + (i++) + ".xml"));
			}

			LogFactory.getLog(this.getClass()).info("Thread executed.");

		}
	}


	private List<ResourceHelper> getRHs() throws WebLabCheckedException {
		final List<ResourceHelper> rhs = new ArrayList<>();
		for (final File file : new File("src/test/resources/corpus").listFiles()) {
			if (file.isFile() && file.getName().endsWith(".xml")) {
				rhs.add(new JenaResourceHelper(new WebLabMarshaller().unmarshal(file, Document.class)));
			}
		}
		return rhs;
	}

}
