/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.gate;

import java.io.File;
import java.net.URI;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.context.support.FileSystemXmlApplicationContext;


public class ServiceTest {


	@Test
	public void testSimpleProcess() throws Exception {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/beans.xml")) {
			final Analyser analyser = fsxac.getBean("analyserImpl", GateService.class);

			final ProcessArgs pa = new ProcessArgs();
			pa.setUsageContext("");

			final Document docEng = this.createEnglishDoc(true);
			final Document docFre = this.createFrenchDoc(true);
			final Document docEngNoLang = this.createEnglishDoc(false);


			pa.setResource(docEngNoLang);
			analyser.process(pa);
			Assert.assertNotNull(pa.getResource());
			Assert.assertEquals(pa.getResource(), docEngNoLang);
			Assert.assertEquals(1, docEngNoLang.getMediaUnit().size());
			Assert.assertEquals(0, docEngNoLang.getMediaUnit().get(0).getSegment().size());
			Assert.assertEquals(0, docEngNoLang.getMediaUnit().get(0).getAnnotation().size());


			pa.setResource(docEng);
			analyser.process(pa);
			Assert.assertNotNull(pa.getResource());
			Assert.assertEquals(pa.getResource(), docEng);
			Assert.assertEquals(1, docEng.getMediaUnit().size());
			final MediaUnit medEng = docEng.getMediaUnit().get(0);
			Assert.assertTrue(medEng.getSegment().size() > 0);
			Assert.assertEquals(2, medEng.getAnnotation().size());
			final Annotation annotEng = medEng.getAnnotation().get(1);
			Assert.assertEquals("http://weblab.ow2.org/services/gate-extraction", new WProcessingAnnotator(URI.create(annotEng.getUri()), annotEng).readProducedBy().firstTypedValue().toString());
			new WebLabMarshaller().marshalResource(docEng, new File("target/res-eng.xml"));


			pa.setResource(docFre);
			analyser.process(pa);
			Assert.assertNotNull(pa.getResource());
			Assert.assertEquals(pa.getResource(), docFre);
			Assert.assertEquals(1, docFre.getMediaUnit().size());
			final MediaUnit medFr = docFre.getMediaUnit().get(0);
			Assert.assertTrue(medFr.getSegment().size() > 0);
			Assert.assertEquals(2, medFr.getAnnotation().size());
			final Annotation annotFr = medFr.getAnnotation().get(1);
			Assert.assertEquals("http://weblab.ow2.org/services/gate-extraction", new WProcessingAnnotator(URI.create(annotFr.getUri()), annotFr).readProducedBy().firstTypedValue().toString());
			new WebLabMarshaller().marshalResource(docFre, new File("target/res-fre.xml"));
		}
	}


	@Test
	public void testMultiMediaUnitProcess() throws Exception {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/beans.xml")) {
			final Analyser analyser = fsxac.getBean("analyserImpl", GateService.class);

			final ProcessArgs pa = new ProcessArgs();
			pa.setUsageContext("");

			final Document doc = new WebLabMarshaller(true).unmarshal(new File("src/test/resources/multi_lang.xml"), Document.class);

			pa.setResource(doc);
			analyser.process(pa);
			Assert.assertNotNull(pa.getResource());
			Assert.assertEquals(pa.getResource(), doc);

			new WebLabMarshaller().marshalResource(doc, new File("target/res-multi_lang.xml"));
		}
	}


	@Test
	public void testMultiMediaUnitProcessWithRTController() throws Exception {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/beans-RT.xml")) {
			final Analyser analyser = fsxac.getBean("analyserImpl", GateService.class);

			final ProcessArgs pa = new ProcessArgs();
			pa.setUsageContext("");

			final Document doc = new WebLabMarshaller(true).unmarshal(new File("src/test/resources/multi_lang.xml"), Document.class);

			pa.setResource(doc);
			analyser.process(pa);
			Assert.assertNotNull(pa.getResource());
			Assert.assertEquals(pa.getResource(), doc);

			new WebLabMarshaller().marshalResource(doc, new File("target/res-multi_lang.xml"));
		}
	}


	private Document createEnglishDoc(final boolean withLang) {
		final Document doc = WebLabResourceFactory.createResource("SmallEnglishTest", Long.toString(System.nanoTime()), Document.class);
		final Text text = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		text.setContent("WebLab: An integration infrastructure to ease the development of multimedia processing applications\n"
				+ "Patrick GIROUX, Stephan BRUNESSAUX, Sylvie BRUNESSAUX, Jérémie DOUCY, Gérard DUPONT, Bruno GRILHERES, Yann MOMBRUN, Arnaud SAVAL\n"
				+ "Information Processing Control and Cognition (IPCC)\n" + "EADS Defence and Security Systems\n" + "Parc d'Affaire des Portes\n" + "27106 Val de Reuil\n"
				+ "http://weblab-project.org\n" + "ipcc@weblab-project.org\n"
				+ "{patrick.giroux, stephan.brunessaux, sylvie.brunessaux, jeremie.doucy, gerard.dupont, bruno.grilheres, yann.mombrun, arnaud.saval}@eads.com\n" + "Abstract:\n"
				+ "In this paper, we introduce the EADS' WebLab platform (http://weblab-project.org) that aims at providing an integration infrastructure for multimedia "
				+ "information processing components. In the following, we explain the motivations that have led to the realisation of this project within EADS and the "
				+ "requirements that have led our choices. After a quick review of existing information processing platforms, we present the chosen service oriented "
				+ "architecture, and the three layers of the WebLab project (infrastructure, services and applications).\n"
				+ "Then, we detail the chosen exchange model and normalised services interfaces that enable semantic interoperability between information processing "
				+ "components. We present the technical choices made to guarantee technical interoperability between the components by the use of an Enterprise Service Bus (ESB).\n"
				+ "Moreover, we present the orchestration and portal mechanisms that we have added to the WebLab to enable architects to quickly build multimedia "
				+ "processing applications. In the following, we illustrate the integration process by describing three applications that have been developed on top of "
				+ "this architecture on three R&D projects (Vitalas, WebContent and eWok-Hub). Finally, we propose some perspectives such as the realisation of an information "
				+ "processing services directory, or a toolkit following MDA (Model Driven Architecture) approach to ease the integration process.\n" + "Keywords:\n"
				+ "Integration infrastructure, Service Oriented Architecture, Semantics, Multimedia Information Processing Platform.");
		if (withLang) {
			new DublinCoreAnnotator(doc).writeLanguage("en");
			new DublinCoreAnnotator(text).writeLanguage("en");
		}
		return doc;
	}


	private Document createFrenchDoc(final boolean withLang) {
		final Document doc = WebLabResourceFactory.createResource("SmallFrenchTest", Long.toString(System.nanoTime()), Document.class);
		final Text text = WebLabResourceFactory.createAndLinkMediaUnit(doc, Text.class);
		text.setContent("Lionel Messi désigné joueur FIFA de l'année 2009 \n" + "Sur la planète football, l'année 2009 est décidément l'année Messi. "
				+ "L'Argentin du FC Barcelone, déjà sacré Ballon d'or France Football 2009, a été désigné joueur de l'année par la Fédération internationale (FIFA) sur "
				+ "la base du vote des sélectionneurs et des capitaines des équipes nationales, lundi 21 décembre à Zurich \n"
				+ " Sans surprise, Lionel Messi s'offre donc un prestigieux doublé après le Ballon d'or obtenu le 1er décembre. Le joueur de Barcelone, âgé de 22 ans, qui a remporté samedi"
				+ " la Coupe du monde des clubs avec le Barça – son sixième titre en 2009 –, était en concurrence "
				+ "avec quatre autres joueurs : les Espagnols du FC Barcelone Andres Iniesta et Xavi, le Brésilien"
				+ " Kaka (Real Madrid) et le Portugais Cristiano Ronaldo (Real Madrid), vainqueur l'an dernier.");
		if (withLang) {
			new DublinCoreAnnotator(doc).writeLanguage("fr");
			new DublinCoreAnnotator(text).writeLanguage("fr");
		}
		return doc;
	}

}
