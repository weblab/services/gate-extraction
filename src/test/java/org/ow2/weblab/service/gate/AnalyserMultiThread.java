/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.gate;

import java.util.concurrent.Callable;

import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;


public class AnalyserMultiThread implements Callable<Resource> {


	private final Analyser a;


	final private Document doc;


	final private String uc;


	public AnalyserMultiThread(final Analyser a, final Document doc, final String uc) {
		this.a = a;
		this.doc = doc;
		this.uc = uc;
	}


	@Override
	public Resource call() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(this.doc);
		pa.setUsageContext(this.uc);
		final ProcessReturn pr = this.a.process(pa);
		return pr.getResource();
	}

}
