/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.service.gate;

import gate.CorpusController;

import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabUncheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.service.gate.converter.GateToWebLab;
import org.springframework.context.support.FileSystemXmlApplicationContext;


/**
 * @author ymombrun
 */
public class ExceptionTest {


	@Test(expected = InvalidParameterException.class)
	public void testNullProcessArgs() throws Exception {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/beans.xml")) {
			final GateService analyser = new GateService(fsxac.getBean("myController", CorpusController.class));
			analyser.process(null);
		}
	}


	@Test(expected = InvalidParameterException.class)
	public void testNullResource() throws Exception {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/beans.xml")) {
			final GateService analyser = new GateService(fsxac.getBean("myController", CorpusController.class));
			analyser.process(new ProcessArgs());
		}
	}


	@Test(expected = InvalidParameterException.class)
	public void testNonMediaUnitResource() throws Exception {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/beans.xml")) {
			final GateService analyser = new GateService(fsxac.getBean("myController", CorpusController.class));
			final ProcessArgs pa = new ProcessArgs();
			pa.setResource(WebLabResourceFactory.createResource("test", Long.toString(System.nanoTime()), PieceOfKnowledge.class));
			analyser.process(pa);
		}
	}

	@Test(expected = WebLabUncheckedException.class)
	public void testWithoutControllers() throws Exception {
		try (final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/test/resources/beans.xml")) {
			final GateService analyser = new GateService(fsxac.getBean("myController", CorpusController.class), new GateToWebLab(), 1, 0);
			final ProcessArgs pa = new ProcessArgs();
			pa.setResource(WebLabResourceFactory.createResource("test", Long.toString(System.nanoTime()), PieceOfKnowledge.class));
			analyser.process(pa);
		}
	}

}
